using System;
public class User
    {
        private string name;

        private string surname;

        /// <summary>
        /// Constructor for User class
        /// </summary>
        /// <param name="name">User's name</param>
        /// <param name="surname">User's surname</param>
        public User(string name, string surname) 
        {
            this.name = name;
            this.surname = surname;
        }

        public string Name
        {
            get
            {
                return name;
            }
        }

        public string Surname
        {
            get
            {
                return surname;
            }
        }
        /// <summary>
        /// Creates a string for output
        /// </summary>
        /// <returns>string value</returns>
        public override string ToString() 
        {
            return name + " " + surname; 
        }
    }