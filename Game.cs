
public class Game
    {
        
        private string gameName;
        private string genre;

        /// <summary>
        /// Constructor for Game class
        /// </summary>
        /// <param name="name"> Game's name </param>
        /// <param name="genre"> Game's genre </param>
        public Game(string name, string genre) 
        {
            this.gameName = name;
            this.genre = genre;
        }

        /// <summary>
        /// Get the game's name
        /// </summary>
        /// <returns>Game's name</returns>
        public string GetName() 
        {
            return gameName;
        }

        /// <summary>
        /// Get the game's genre
        /// </summary>
        /// <returns>Game's genre</returns>
        public string GetGenre()
        {
            return this.genre;
        }
    }