using System;
using System.Collections.Generic;
public class Gamer : User 
    {
    
        private List<PlatformLibrary> librarys = new List<PlatformLibrary>();
        private string nickname;
        public Gamer(string name, string surname, string nickname) 
            :base(name, surname)
            {
                this.nickname = nickname;
            }

        public string Nickname
        {
            get
            {
                return nickname;
            }
            set
            {
                nickname = value;
            }
        }
        
        public void AddGamesLibrary(PlatformLibrary lib) 
        {
            librarys.Add(lib);
        }

        public PlatformLibrary GetLibrary(string platform)
        {
            return librarys.Find(library => library.Platform.Equals(platform));   
        }
        public List<Game> SearchForGame(string gameName) 
        {
            List<Game> foundGames = new List<Game>();
            foreach(PlatformLibrary lib in librarys)
            {
                foundGames.AddRange(lib.SearchForGame(gameName));
            }
            return foundGames;
        }

        public override string ToString() 
        {
            return this.Name + " \"" + nickname + "\" " + this.Surname; 
        }
    }