using System;
using System.Collections.Generic;
using System.IO;
public class PlatformLibrary
    {
        private List<Game> games = new List<Game>();
        private string platform;

        public PlatformLibrary(string platform) 
        {
            this.platform = platform;
        }

        public PlatformLibrary(Game game, string platform) 
        {
            this.platform = platform;
            AddGame(game);
        }
        public string Platform
        {
            get
            {
                return this.platform;
            }
        }
        public void AddGame(Game game) 
        {
            games.Add(game);
        }

        /// <summary>
        /// Returns number of games in library
        /// </summary>
        /// <returns>count</returns>
        public int Count()
        {
            return games.Count;
        }

        /// <summary>
        /// Find game(s) in library
        /// </summary>
        /// <param name="gameName"> Game's name</param>
        /// <returns>List of founded games</returns>
        public List<Game> SearchForGame(string gameName)
        {
            return games.FindAll(game => game.GetName().Equals(gameName));
        }

        /// <summary>
        /// Import games from file
        /// </summary>
        /// <param name="path">file's path</param>
        public void AddGamesFromFile(string path) 
        {
            try 
            {
                StreamReader sr = new StreamReader(path, System.Text.Encoding.Default);
                string line;
                while((line = sr.ReadLine()) != null)
                {
                    int separatorIndex = line.IndexOf("|");
                    games.Add(new Game(line.Substring(0, separatorIndex), line.Substring(separatorIndex + 1, line.Length - separatorIndex - 1)));
                }
            }
            catch(IOException)
            {
                Console.WriteLine("Exception");
            }
        }
        
        /// <summary>
        /// Export games to file
        /// </summary>
        /// <param name="path">file's path</param>
        public void ExportGamesToFile(string path)
        {
            try
            {
                StreamWriter sw = new StreamWriter(path);
                foreach(Game game in games)
                {
                    sw.WriteLine(game.GetName() + "|" + game.GetGenre());
                }
            }
            catch(IOException)
            {
                Console.WriteLine("Exception");
            }

        }

        public void PrintGames() {
            foreach(Game game in games)
            {
                Console.WriteLine("{0} is {1}", game.GetName(), game.GetGenre());
            }
        }
    }